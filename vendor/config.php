<?php defined('BASEPATH') OR exit('<pre>Error Code 999: No direct script access allowed!</pre>');
define('CONFIG', array(
    'username' => 'serveradmin',
    'password' => 'passwd',
    'host' => '151.80.110.200',
    'query_port' => '10011',
    'server_port' => 9987,
    'bot_username' => 'Xaron"s Evanes',
    'banners' => array(
        './public/banners/banner.png',
        './public/banners/banner2.png'
    ),
    'fonts' => array(
        'normal' => './public/fonts/CaviarDreams.ttf',
        'bold' => './public/fonts/CaviarDreams_Bold.ttf'
    ),
    'weather' => array(
        'weathermap' => array(
            'status' => false,
        ),
        'apixu' => array(
            'status' => false,
            'api_key' => ''
        ),
    ),
    'development' => array(
        'status' => false
    ),
    'server_tracker' => false,
    'proxy_check' => false
));
define('APP_VERSION', base64_decode('MS4wLjQ='));
define('APP_DEV_VERSION', base64_decode('MS4wLjM='));
?>